#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml bugs\n"
"PO-Revision-Date: 2015-08-05 16:19+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "trong gói"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "được đánh thẻ"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "với tính nghiêm trọng"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "trong gói nguồn"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "trong các gói bảo trì bởi"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "báo cáo bởi"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "sở hữu bởi"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "với trạng thái"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "với thư gửi từ"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "lỗi mới nhất"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "với chủ đề có chứa"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "với trạng thái treo"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "với người gửi có chứa"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "với chuyển tiếp có chứa"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "với chủ sở hữu có chứa"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "với gói"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "bình thường"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "kiểu cũ"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "thô"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "tuổi"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Lặp hòa trộn"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Đảo ngược các lỗi"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Đảo ngược tính treo"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Đảo ngược tính nghiêm trọng"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Không lỗi cái mà các gói ảnh hưởng"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "Không"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "thử nghiệm"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "bản ổn định cũ"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "bản ổn định"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "bản thử nghiệm"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "bản chưa ổn định"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Không lưu trữ"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Lưu trữ"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Lưu và không lưu trữ"
