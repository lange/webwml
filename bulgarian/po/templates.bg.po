#
# Damyan Ivanov <dmn@debian.org>, 2011, 2012, 2013, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.61\n"
"PO-Revision-Date: 2015-04-02 17:44+0300\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: Bulgarian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 2.91.6\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Сайт на проекта Дебиан"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Търсене в сайта на Дебиан."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Дебиан"

#: ../../english/template/debian/basic.wml:46
msgid "Debian website search"
msgstr "Търсене в сайта на Дебиан"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Да"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Не"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Проект Дебиан"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Дебиан е операционна система и дистрибуция на свободен софтуер.Тя се "
"поддържа и обновява с усилията и времето на много доброволци."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, отворен код, свободен, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr ""
"Обратно към <a href=\"m4_HOME/\">началната страница на проекта Дебиан</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Начална страница"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Прескачане на бързите връзки"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "За нас"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "За Дебиан"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Контакти"

#: ../../english/template/debian/common_translation.wml:37
msgid "Donations"
msgstr "Дарения"

#: ../../english/template/debian/common_translation.wml:40
msgid "Events"
msgstr "Събития"

#: ../../english/template/debian/common_translation.wml:43
msgid "News"
msgstr "Новини"

#: ../../english/template/debian/common_translation.wml:46
msgid "Distribution"
msgstr "Дистрибуция"

#: ../../english/template/debian/common_translation.wml:49
msgid "Support"
msgstr "Поддръжка"

#: ../../english/template/debian/common_translation.wml:52
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:55
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Разработка"

#: ../../english/template/debian/common_translation.wml:58
msgid "Documentation"
msgstr "Документация"

#: ../../english/template/debian/common_translation.wml:61
msgid "Security Information"
msgstr "За сигурността"

#: ../../english/template/debian/common_translation.wml:64
msgid "Search"
msgstr "Търсене"

#: ../../english/template/debian/common_translation.wml:67
msgid "none"
msgstr "няма"

#: ../../english/template/debian/common_translation.wml:70
msgid "Go"
msgstr "Отиване"

#: ../../english/template/debian/common_translation.wml:73
msgid "worldwide"
msgstr "за целия свят"

#: ../../english/template/debian/common_translation.wml:76
msgid "Site map"
msgstr "Карта на сайта"

#: ../../english/template/debian/common_translation.wml:79
msgid "Miscellaneous"
msgstr "Разни"

#: ../../english/template/debian/common_translation.wml:82
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Снабдяване с Дебиан"

#: ../../english/template/debian/common_translation.wml:85
msgid "The Debian Blog"
msgstr "Блога на Дебиан"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Изпращайте всички коментари, критики и предложения за тези страници на нашия "
"<a href=\"mailto:debian-doc@lists.debian.org\">пощенски списък</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "не се нуждае"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "не е достъпно"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "няма"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "във версия 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "във версия 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "във версия 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "във версия 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "във версия 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:105
#, fuzzy
#| msgid ""
#| "To report a problem with the web site, e-mail <a href=\"mailto:debian-"
#| "www@lists.debian.org\">debian-www@lists.debian.org</a>.  For other "
#| "contact information, see the Debian <a href=\"m4_HOME/contact\">contact "
#| "page</a>. Web site source code is <a href=\"m4_HOME/devel/website/"
#| "using_cvs\">available</a>."
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Ако сте открили проблем в сайта, пишете до <a href=\"mailto:debian-www@lists."
"debian.org\">debian-www@lists.debian.org</a> (на английски). Ако желаете да "
"ни пишете за друго, вижте страницата <a href=\"m4_HOME/contact\">Контакти</"
"a>. <a href=\"m4_HOME/devel/website/using_cvs\">Информация за достъп до "
"изходния код</a> на сайта."

#: ../../english/template/debian/footer.wml:108
msgid "Last Modified"
msgstr "Последна промяна"

#: ../../english/template/debian/footer.wml:111
msgid "Copyright"
msgstr "Copyright"

#: ../../english/template/debian/footer.wml:114
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> и други;"

#: ../../english/template/debian/footer.wml:117
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "<a href=\"m4_HOME/license\" rel=\"copyright\">Лицензни условия</a>"

#: ../../english/template/debian/footer.wml:120
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian е регистрирана <a href=\"m4_HOME/trademark\">търговска марка</a> на "
"Software in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Тази страница е налична и на следните езици:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Настройване на <a href=m4_HOME/intro/cn>езика по подразбиране</a>"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Дебиан Интернационал"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Партньори"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Седмични новини за Дебиан"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Седмични новини"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Новини за проекта Дебиан"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Новини за проекта"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Издания"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Пакети"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Изтегляне"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Дебиан на компактдиск"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Книги за Дебиан"

#: ../../english/template/debian/links.tags.wml:37
#, fuzzy
#| msgid "Debian"
msgid "Debian Wiki"
msgstr "Дебиан"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Архиви на пощенските списъци"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Пощенски списъци"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Обществен договор"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Кодекс"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Дебиан 5.0 - Универсалната операционна система"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Карта на сайта"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "База данни на програмистите"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "Въпроси за Дебиан"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Политики на Дебиан"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Справочник на програмиста"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Ръководство за нови отговорници"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Критични грешки в предстоящото издание"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Доклади от Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Архиви на потребителски пощенски списъци"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Архиви на пощенски списъци за програмисти"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Архиви на пощенски списъци за преводи и локализация"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Архиви на пощенски списъци за портове"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Архиви на пощенските списъци на Системата за следене на грешките"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Архиви на други пощенски списъци"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Свободен софтуер "

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Разработка"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Помощ за Дебиан"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Доклади за грешки"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Портове и Архитектури"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Ръководство за инсталиране"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Търговци на дискове"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "Образи на оптични/USB носители"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Мрежова инсталация"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Компютър с Дебиан"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Проект Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Контрол на качеството"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Система за следене на пакети"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Резюме на пакетите на разработчика"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Начална страница"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Няма намерени за тази година."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "предложение"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "дискутира се"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "гласуване"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "завършен"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "оттеглен"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Бъдещи събития"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Минали събития"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(нова ревизия)"

#: ../../english/template/debian/recent_list.wml:303
msgid "Report"
msgstr "Доклад"

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s за %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Забележка:</em> <a href=\"$link\">Оригиналният документ</a> има по-нова "
"версия от превода."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Внимание! Този превод е твърде стар, моля прочетете  <a href=\"$link"
"\">оригиналния документ</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr ""
"<em>Бележка:</em> Оригиналния документ на този превод вече не съществува."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:7
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Обратно към страницата <a href=\"../\">Кой използва Дебиан?</a>."

#~ msgid "Visit the site sponsor"
#~ msgstr "Посетете спонсорите на сайта"

#~ msgid "Select a server near you"
#~ msgstr "Изберете сървър близо до вас"

#~ msgid "Have you found a problem with the site layout?"
#~ msgstr "Страницата изглежда странно?"

#~ msgid "Report it!"
#~ msgstr "Съобщаване"

#~ msgid "List of Consultants"
#~ msgstr "Списък на консултантите"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Обратно към страницата на консултантите на Debian"

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "Download with Jigdo"
#~ msgstr "Сваляне чрез Jigdo"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "Сваляне чрез HTTP/FTP"

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Купуване на CD и DVD дискове"

#~ msgid "Network Install"
#~ msgstr "Мрежова инсталация"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Сваляне"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Други"

#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_artwork\" />Картинки"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Огледала"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Rsync Огледала"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Сваляне чрез Torrent"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Инфо за версията на image файловете"

#~ msgid "Debian CD team"
#~ msgstr "Debian CD екип"

#~ msgid "debian_on_cd"
#~ msgstr "debian_на_cd"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />faq"

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "buy"
#~ msgstr "купи"

#~ msgid "net_install"
#~ msgstr "net_install"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "<void id=\"misc-bottom\" />други"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "<a href=\"/MailingLists/disclaimer\">публични пощенски списъци</a> за "
#~ "англоговорящи отностно CDs/DVDs:"

#~ msgid "Date"
#~ msgstr "Дата"

#~ msgid "Time Line"
#~ msgstr "Времева линия"

#~ msgid "Nominations"
#~ msgstr "Номинации"

#~ msgid "Debate"
#~ msgstr "Дебат"

#~ msgid "Platforms"
#~ msgstr "Платформи"

#~ msgid "Proposer"
#~ msgstr "Инициатор"

#~ msgid "Proposal A Proposer"
#~ msgstr "Предложение А Инициатор"

#~ msgid "Proposal B Proposer"
#~ msgstr "Предложение Б Инициатор"

#~ msgid "Proposal C Proposer"
#~ msgstr "Предложение В Инициатор"

#~ msgid "Proposal D Proposer"
#~ msgstr "Предложение Г Инициатор"

#~ msgid "Proposal E Proposer"
#~ msgstr "Предложение Д Инициатор"

#~ msgid "Proposal F Proposer"
#~ msgstr "Предложение Е Предложител"

#~ msgid "Seconds"
#~ msgstr "Арбитри"

#~ msgid "Proposal A Seconds"
#~ msgstr "Предложение А Арбитри"

#~ msgid "Proposal B Seconds"
#~ msgstr "Предложение Б Арбитри"

#~ msgid "Proposal C Seconds"
#~ msgstr "Предложение В Арбитри"

#~ msgid "Proposal D Seconds"
#~ msgstr "Предложение В Арбитри"

#~ msgid "Proposal E Seconds"
#~ msgstr "Предложение Г Арбитри"

#~ msgid "Proposal F Seconds"
#~ msgstr "Предложение Д Арбитри"

#~ msgid "Opposition"
#~ msgstr "Опозиция"

#~ msgid "Text"
#~ msgstr "Текст"

#~ msgid "Proposal A"
#~ msgstr "Предложение А"

#~ msgid "Proposal B"
#~ msgstr "Предложение Б"

#~ msgid "Proposal C"
#~ msgstr "Предложение В"

#~ msgid "Proposal D"
#~ msgstr "Предложение Г"

#~ msgid "Proposal E"
#~ msgstr "Предложение Д"

#~ msgid "Proposal F"
#~ msgstr "Предложение Е"

#~ msgid "Choices"
#~ msgstr "Възможности"

#~ msgid "Amendment Proposer"
#~ msgstr "Предложил Изменение"

#~ msgid "Amendment Seconds"
#~ msgstr "Арбитри по изменението"

#~ msgid "Amendment Text"
#~ msgstr "Текст на изменението"

#~ msgid "Amendment Proposer A"
#~ msgstr "Предложител за Изменение A"

#~ msgid "Amendment Seconds A"
#~ msgstr "Арбитри по изменението A"

#~ msgid "Amendment Text A"
#~ msgstr "Текст на изменението A"

#~ msgid "Amendment Proposer B"
#~ msgstr "Предложител за Изменение B"

#~ msgid "Amendment Seconds B"
#~ msgstr "Арбитри по изменението B"

#~ msgid "Amendment Text B"
#~ msgstr "Текст на изменението B"

#~ msgid "Amendments"
#~ msgstr "Вносители на изменението"

#~ msgid "Proceedings"
#~ msgstr "В процес"

#~ msgid "Majority Requirement"
#~ msgstr "Изискване за мнозинство"

#~ msgid "Data and Statistics"
#~ msgstr "Данни и Статистики"

#~ msgid "Quorum"
#~ msgstr "Кворум"

#~ msgid "Minimum Discussion"
#~ msgstr "Минимум Дискусия"

#~ msgid "Ballot"
#~ msgstr "Балотаж"

#~ msgid "Forum"
#~ msgstr "Форум"

#~ msgid "Outcome"
#~ msgstr "Решение"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Чака&nbsp;за&nbsp;спонсори"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "В&nbsp;дискусия"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Отворен за&nbsp;гласуване"

#~ msgid "Decided"
#~ msgstr "Решен"

#~ msgid "Withdrawn"
#~ msgstr "Върнат"

#~ msgid "Other"
#~ msgstr "Друг"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Главна&nbsp;страница&nbsp;за гласуване"

#~ msgid "How&nbsp;To"
#~ msgstr "Стъпка по&nbsp;стъпка"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Изпращане на&nbsp;предложение"

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Редактиране на&nbsp;предложение"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Проследяване на&nbsp;предложение"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Прочитане на&nbsp;резултат"

#~ msgid "Vote"
#~ msgstr "Гласуване"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "Прочетете страницата с <a href=\"./\">информация за лиценза</a> за "
#~ "обобщен поглед върху лицензните споразумения на Debian - Debian License "
#~ "Summaries (DLS)."

#~ msgid "Not Redistributable"
#~ msgstr "Не може да се разпространява"

#~ msgid "Non-Free"
#~ msgstr "Не-свободен"

#~ msgid "Free"
#~ msgstr "Свободен"

#~ msgid "not redistributable"
#~ msgstr "не може да се разпространява"

#~ msgid "non-free"
#~ msgstr "не-свободен"

#~ msgid "free"
#~ msgstr "свободен"

#~ msgid "License text"
#~ msgstr "Текст на лиценза"

#~ msgid "License text (translated)"
#~ msgstr "Текст на лиценза (преведен)"

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "Това резюме бе подготвено от <summary-author/>."

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "Оригиналното резюме на <summary-author/> може да бъде прочетено в <a href="
#~ "\"<summary-url/>\">архива на списъците</a>."

#~ msgid "Original Summary"
#~ msgstr "Оригинално резюме"

#~ msgid "Discussion"
#~ msgstr "Дискусия"

#~ msgid "Justification"
#~ msgstr "Подравнение"

#~ msgid "Summary"
#~ msgstr "Резюме"

#~ msgid "Version"
#~ msgstr "Версия"

#~ msgid "License"
#~ msgstr "Лиценз"

#~ msgid "Date published"
#~ msgstr "Дата на публикуване"

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, Версия %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s  &ndash; %s: %s"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Debian-Legal Архив"

#~ msgid "DFSG FAQ"
#~ msgstr "DFSG FAQ"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DLS Index"
#~ msgstr "DLS Индекс"

#~ msgid "License Information"
#~ msgstr "Информация за лиценза"

#~ msgid "package info"
#~ msgstr "инфо за пакета"

#~ msgid "requested %s days ago."
#~ msgstr "заявен преди %s дни."

#~ msgid "requested yesterday."
#~ msgstr "заявен вчера."

#~ msgid "requested today."
#~ msgstr "заявен днес."

#~ msgid "%s days in preparation."
#~ msgstr "%s дни в процес на подготовка."

#~ msgid "in preparation since yesterday."
#~ msgstr "в процес на подготовка от вчера."

#~ msgid "in preparation since today."
#~ msgstr "в процес на подготовка от днес."

#~ msgid "%s days in adoption."
#~ msgstr "%s дни в процес на адаптиране."

#~ msgid "in adoption since yesterday."
#~ msgstr "в процес на адаптиране от вчера."

#~ msgid "in adoption since today."
#~ msgstr "в процес на адаптиране от днес."

#~ msgid "No help requested"
#~ msgstr "Няма проявявено желание за помощ"

#~ msgid "No Requested packages"
#~ msgstr "Няма заявени пакети"

#~ msgid "No packages waiting to be packaged"
#~ msgstr "Няма пакети, чакащи да бъдат пакетирани"

#~ msgid "No packages waiting to be adopted"
#~ msgstr "Няма пакети, чакащи за адаптиране"

#~ msgid "No orphaned packages"
#~ msgstr "Няма изоставени пакети"

#~ msgid "No requests for adoption"
#~ msgstr "Няма заявления за адаптиране"

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Debian седмични новини се радактира от <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Debian седмични новини бяха редактирани от <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Debian седмични новини се радактира от <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Debian седмични новини се редактира от <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "За да получавате това писмо с новини всяка седмица на Вашият e-mail, <a "
#~ "href=\"https://lists.debian.org/debian-news/\">запишете се в debian-news "
#~ "пощенския списък</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Обратно към <a href=\"./\">страницата за Debian лекторите</a>."

#~ msgid "List of Speakers"
#~ msgstr "Списък на Лекторите"

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "<void id=\"pluralfemale\" />Този текст бе преведен от %s."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "<void id=\"singularfemale\" />Този текст бе преведен от %s."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "<void id=\"plural\" />Този текст бе преведен от %s."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "<void id=\"singular\" />Този текст бе преведен от %s."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Този брой на новините за Debian Проекта бе "
#~ "редактиран от <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Този брой на новините за Debian Проекта бе "
#~ "редактиран от <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Debian Новините на Проекта се редактират от <a href="
#~ "\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Debian Новините на Проекта се редактират от <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr ""
#~ "Налични са <a href=\"../../\">по-стари броеве</a> от това писмо с новини."

#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "За да получавате това писмо с новини всяка седмица, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">абонирайте се за debian-news пощенския "
#~ "списък</a>."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (несъществуваща връзка)"

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Обратно към: други  <a href=\"./\">Debian новини</a> || <a href=\"m4_HOME/"
#~ "\">началната страница на Debian проекта</a>."

#~ msgid "Download calendar entry"
#~ msgstr "Сваляне на събитията по дата"

#~ msgid "Latest News"
#~ msgstr "Последни новини"

#~ msgid "Related Links"
#~ msgstr "Връзки към тази тема"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Координатор на</th><th>проекта</th>"

#~ msgid "Main Coordinator"
#~ msgstr "Главен координатор"

#~ msgid "Debian Involvement"
#~ msgstr "Debian Инициативи"

#~ msgid "More Info"
#~ msgstr "Повече инфо"

#~ msgid "Where"
#~ msgstr "Къде"

#~ msgid "When"
#~ msgstr "Кога"

#~ msgid "link may no longer be valid"
#~ msgstr "връзката може да не е валидна вече"

#~ msgid "Upcoming Attractions"
#~ msgstr "Предстоящи събития"

#~ msgid "More information"
#~ msgstr "Повече информация"

#~ msgid "Rating:"
#~ msgstr "Оценка:"

#~ msgid "Nobody"
#~ msgstr "Никого"

#~ msgid "Taken by:"
#~ msgstr "Взет от:"

#~ msgid "More information:"
#~ msgstr "Повече информация:"
