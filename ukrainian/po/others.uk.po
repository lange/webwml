# translation of others.uk.po to
# translation of others.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@univ.kiev.ua>, 2005.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net> 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: others\n"
"PO-Revision-Date: 2017-11-12 23:08+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: Ukrainian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/banners/index.tags:7
msgid "Download"
msgstr "Завантажити"

#: ../../english/banners/index.tags:11
msgid "Old banner ads"
msgstr "Старі рекламні банери"

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr "Працює"

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr "sarge"

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr "sarge (зламаний)"

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr "Завантажується"

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr "Будується"

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr "Іще ні"

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr ""

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr "<void id=\"d-i\" />Невідомо"

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr "Недоступно"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Куточок нових членів"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Крок 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Крок 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Крок 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Крок 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Крок 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Крок 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Крок 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Контрольний список заявника"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Для отримання додаткової інформації дивіться <a href=\"m4_HOME/intl/french/"
"\">https://www.debian.org/intl/french/</a> (доступно лише французькою)."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
msgid "More information"
msgstr "Більше інформації"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Для отримання додаткової інформації дивіться <a href=\"m4_HOME/intl/spanish/"
"\">https://www.debian.org/intl/spanish/</a> (доступно лише іспанською)."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Телефон"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Факс"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Адреса"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "З&nbsp;``Debian''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Без&nbsp;``Debian''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Вбудований PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr ""

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr ""

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr ""

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (мінікнопка)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "те ж саме, що і вище"

#: ../../english/misc/merchandise.def:8
msgid "Products"
msgstr "Товари"

#: ../../english/misc/merchandise.def:11
msgid "T-shirts"
msgstr "футболки"

#: ../../english/misc/merchandise.def:14
msgid "hats"
msgstr "капелюхи"

#: ../../english/misc/merchandise.def:17
msgid "stickers"
msgstr "наклейки"

#: ../../english/misc/merchandise.def:20
msgid "mugs"
msgstr "кружки"

#: ../../english/misc/merchandise.def:23
msgid "other clothing"
msgstr "інший одяг"

#: ../../english/misc/merchandise.def:26
msgid "polo shirts"
msgstr "теніска"

#: ../../english/misc/merchandise.def:29
msgid "frisbees"
msgstr "фрізбі"

#: ../../english/misc/merchandise.def:32
msgid "mouse pads"
msgstr "коврики для миши"

#: ../../english/misc/merchandise.def:35
msgid "badges"
msgstr "значки"

#: ../../english/misc/merchandise.def:38
msgid "basketball goals"
msgstr "баскетбольні корзини"

#: ../../english/misc/merchandise.def:42
msgid "earrings"
msgstr "сережки"

#: ../../english/misc/merchandise.def:45
msgid "suitcases"
msgstr "валізи"

#: ../../english/misc/merchandise.def:48
msgid "umbrellas"
msgstr "парасолі"

#: ../../english/misc/merchandise.def:51
msgid "pillowcases"
msgstr "наволочки"

#: ../../english/misc/merchandise.def:54
msgid "keychains"
msgstr "брелоки"

#: ../../english/misc/merchandise.def:57
msgid "Swiss army knives"
msgstr "швейцарські армійські ножі"

#: ../../english/misc/merchandise.def:60
msgid "USB-Sticks"
msgstr "USB-носії"

#: ../../english/misc/merchandise.def:75
msgid "lanyards"
msgstr "ремінці"

#: ../../english/misc/merchandise.def:78
msgid "others"
msgstr "інше"

#: ../../english/misc/merchandise.def:98
msgid "Donates money to Debian"
msgstr "Жертвує гроші Debian"

#: ../../english/misc/merchandise.def:102
msgid "Money is used to organize local free software events"
msgstr ""
"Гроші використовуються для організації місцевих подій, присвячених вільному "
"ПЗ"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Як давно ви користуєтесь Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Ви розробник Debian?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Якими областями Debian Ви займаєтесь?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Що зацікавило Вас у роботі з Debian?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Трішки про тебе..."

#: ../../english/y2k/l10n.data:6
msgid "OK"
msgstr "Гаразд"

#: ../../english/y2k/l10n.data:9
msgid "BAD"
msgstr ""

#: ../../english/y2k/l10n.data:12
msgid "OK?"
msgstr "Гаразд?"

#: ../../english/y2k/l10n.data:15
msgid "BAD?"
msgstr ""

#: ../../english/y2k/l10n.data:18
msgid "??"
msgstr "??"

#: ../../english/y2k/l10n.data:21
msgid "Unknown"
msgstr "Невідомо"

#: ../../english/y2k/l10n.data:24
msgid "ALL"
msgstr "Всі"

#: ../../english/y2k/l10n.data:27
msgid "Package"
msgstr "Пакунок"

#: ../../english/y2k/l10n.data:30
msgid "Status"
msgstr "Статус"

#: ../../english/y2k/l10n.data:33
msgid "Version"
msgstr "Версія"

#: ../../english/y2k/l10n.data:36
msgid "URL"
msgstr "URL"
