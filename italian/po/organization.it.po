# Debian Web Site. organization.it.po
# Copyright (C) 2002 Giuseppe Sacco.
# Giuseppe Sacco <eppesuigoccas@libero.it>, 2003.
# Giuseppe Sacco <eppesuig@debian.org>, 2004, 2005.
# Luca Monducci <luca.mo@tiscali.it>, 2006 - 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: organization.it\n"
"PO-Revision-Date: 2017-12-26 12:12+0100\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "email di delega"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "email di nomina"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegato"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegata"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "attuale"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "membro"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "manager"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Gestore del rilascio stabile"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "mago"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "presidente"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "assistente"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "segretario"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "Direttori"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:87
msgid "Distribution"
msgstr "Distribuzione"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:229
msgid "Communication and Outreach"
msgstr "Comunicazione e sociale"

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:232
msgid "Publicity team"
msgstr "Team Pubblicità"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:300
msgid "Support and Infrastructure"
msgstr "Supporto e infrastruttura"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr "Debian Pure Blends"

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "Leader"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "Commissione tecnica"

#: ../../english/intro/organization.data:82
msgid "Secretary"
msgstr "Segretario"

#: ../../english/intro/organization.data:90
msgid "Development Projects"
msgstr "Progetti di sviluppo"

#: ../../english/intro/organization.data:91
msgid "FTP Archives"
msgstr "Archivi FTP"

#: ../../english/intro/organization.data:93
msgid "FTP Masters"
msgstr "FTP Master"

#: ../../english/intro/organization.data:99
msgid "FTP Assistants"
msgstr "Assistenti FTP"

#: ../../english/intro/organization.data:104
msgid "FTP Wizards"
msgstr "Maghi FTP"

#: ../../english/intro/organization.data:108
msgid "Backports"
msgstr "Backport"

#: ../../english/intro/organization.data:110
msgid "Backports Team"
msgstr "Team Backport"

#: ../../english/intro/organization.data:114
msgid "Individual Packages"
msgstr "Pacchetti individuali"

#: ../../english/intro/organization.data:115
msgid "Release Management"
msgstr "Gestione del rilascio"

#: ../../english/intro/organization.data:117
msgid "Release Team"
msgstr "Team di rilascio"

#: ../../english/intro/organization.data:130
msgid "Quality Assurance"
msgstr "Controllo qualità"

#: ../../english/intro/organization.data:131
msgid "Installation System Team"
msgstr "Team per il sistema d'installazione"

#: ../../english/intro/organization.data:132
msgid "Release Notes"
msgstr "Note di rilascio"

#: ../../english/intro/organization.data:134
msgid "CD Images"
msgstr "Immagini dei CD"

#: ../../english/intro/organization.data:136
msgid "Production"
msgstr "Produzione"

#: ../../english/intro/organization.data:144
msgid "Testing"
msgstr "Test"

#: ../../english/intro/organization.data:146
msgid "Autobuilding infrastructure"
msgstr "Infrastruttura di compilazione automatica"

#: ../../english/intro/organization.data:148
msgid "Wanna-build team"
msgstr "Team wanna-build"

#: ../../english/intro/organization.data:156
msgid "Buildd administration"
msgstr "Amministrazione di buildd"

#: ../../english/intro/organization.data:175
msgid "Documentation"
msgstr "Documentazione"

#: ../../english/intro/organization.data:180
msgid "Work-Needing and Prospective Packages list"
msgstr ""
"Elenco dei pacchetti richiesti e di quelli che necessitano di manodopera "
"(WNPP)"

#: ../../english/intro/organization.data:183
msgid "Debian Live Team"
msgstr "Team Debian Live"

#: ../../english/intro/organization.data:184
msgid "Ports"
msgstr "Port"

#: ../../english/intro/organization.data:219
msgid "Special Configurations"
msgstr "Configurazioni speciali"

#: ../../english/intro/organization.data:222
msgid "Laptops"
msgstr "Portatili"

#: ../../english/intro/organization.data:223
msgid "Firewalls"
msgstr "Firewall"

#: ../../english/intro/organization.data:224
msgid "Embedded systems"
msgstr "Sistemi embedded"

#: ../../english/intro/organization.data:237
msgid "Press Contact"
msgstr "Contatto per la stampa"

#: ../../english/intro/organization.data:239
msgid "Web Pages"
msgstr "Pagine web"

#: ../../english/intro/organization.data:249
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:254
msgid "Outreach"
msgstr "Coninvolgimento in progetti esterni"

#: ../../english/intro/organization.data:258
msgid "Debian Women Project"
msgstr "Progetto Debian Women"

#: ../../english/intro/organization.data:266
msgid "Anti-harassment"
msgstr "Anti-molestie"

#: ../../english/intro/organization.data:272
msgid "Events"
msgstr "Eventi"

#: ../../english/intro/organization.data:278
msgid "DebConf Committee"
msgstr "Commissione DebConf"

#: ../../english/intro/organization.data:285
msgid "Partner Program"
msgstr "Programma per i partner"

#: ../../english/intro/organization.data:290
msgid "Hardware Donations Coordination"
msgstr "Coordinazione delle donazioni hardware"

#: ../../english/intro/organization.data:303
msgid "User support"
msgstr "Supporto agli utenti"

#: ../../english/intro/organization.data:370
msgid "Bug Tracking System"
msgstr "Sistema di tracciamento dei bug (BTS)"

#: ../../english/intro/organization.data:375
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Amministrazione e archivi delle liste di messaggi"

#: ../../english/intro/organization.data:383
msgid "New Members Front Desk"
msgstr "Accoglienza dei nuovi membri"

#: ../../english/intro/organization.data:389
msgid "Debian Account Managers"
msgstr "Gestori degli account Debian"

#: ../../english/intro/organization.data:393
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Per inviare un messaggio privato a tutti i Gestori degli account Debian, "
"usare la chiave GPG 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:394
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Gestori del Keyring (PGP e GPG)"

#: ../../english/intro/organization.data:397
msgid "Security Team"
msgstr "Team della sicurezza"

#: ../../english/intro/organization.data:409
msgid "Consultants Page"
msgstr "Pagina dei consulenti"

#: ../../english/intro/organization.data:414
msgid "CD Vendors Page"
msgstr "Pagina dei rivenditori di CD"

#: ../../english/intro/organization.data:417
msgid "Policy"
msgstr "Regolamento"

#: ../../english/intro/organization.data:422
msgid "System Administration"
msgstr "Amministrazione del sistema"

#: ../../english/intro/organization.data:423
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Questo è l'indirizzo da utilizzare quando si incontrano problemi in una "
"delle macchine Debian, ivi inclusi i problemi relativi a password o a "
"pacchetti non installati."

#: ../../english/intro/organization.data:432
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Se trovi un problema hardware su una macchina Debian, vedi la pagina delle "
"<a href=\"https://db.debian.org/machines.cgi\">macchine Debian</a>, dovrebbe "
"contenere informazioni sugli amministratori divisi per macchine."

#: ../../english/intro/organization.data:433
msgid "LDAP Developer Directory Administrator"
msgstr "Amministratore dell'elenco LDAP degli sviluppatori"

#: ../../english/intro/organization.data:434
msgid "Mirrors"
msgstr "Mirror"

#: ../../english/intro/organization.data:441
msgid "DNS Maintainer"
msgstr "Gestori del DNS"

#: ../../english/intro/organization.data:442
msgid "Package Tracking System"
msgstr "Sistema di tracciamento dei pacchetti"

#: ../../english/intro/organization.data:444
msgid "Treasurer"
msgstr "Tesoriere"

#: ../../english/intro/organization.data:450
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Richieste d'uso del <a name=\"trademark\" href=\"m4_HOME/trademark\">marchio "
"registrato</a>"

#: ../../english/intro/organization.data:453
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Amministratori di Alioth"

#: ../../english/intro/organization.data:457
msgid "Alioth administrators"
msgstr "Amministratori di Alioth"

#: ../../english/intro/organization.data:470
msgid "Debian for children from 1 to 99"
msgstr "Debian per bambini da 1 a 99 anni"

#: ../../english/intro/organization.data:473
msgid "Debian for medical practice and research"
msgstr "Debian per la pratica e la ricerca medica"

#: ../../english/intro/organization.data:476
msgid "Debian for education"
msgstr "Debian nell'educazione"

#: ../../english/intro/organization.data:481
msgid "Debian in legal offices"
msgstr "Debian negli uffici legali"

#: ../../english/intro/organization.data:485
msgid "Debian for people with disabilities"
msgstr "Debian per disabili"

#: ../../english/intro/organization.data:489
msgid "Debian for science and related research"
msgstr "Debian per la scienza e la ricerca scientifica"

#: ../../english/intro/organization.data:492
msgid "Debian for astronomy"
msgstr "Debian per l'astronomia"

#~ msgid "Handhelds"
#~ msgstr "Palmari"

#~ msgid "Publicity"
#~ msgstr "Pubblicità"

#~ msgid "current Debian Project Leader"
#~ msgstr "l'attuale Leader del Progetto Debian"

#~ msgid "Testing Security Team"
#~ msgstr "Team della sicurezza per testing"

#~ msgid "Live System Team"
#~ msgstr "Team per il sistema live"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Manutentore del portachiavi Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Consiglio d'amministrazione della DebConf"

#~ msgid "Bits from Debian"
#~ msgstr "Notizie in pillole da Debian"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Amministratori Summer of Code 2013"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (Non attivo: non rilasciato con squeeze)"

#~ msgid "Volatile Team"
#~ msgstr "Team Volatile"

#~ msgid "Vendors"
#~ msgstr "Rivenditori"

#~ msgid "Security Audit Project"
#~ msgstr "Progetto di verifica della sicurezza"

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordinazione della firma delle chiavi"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Custom Debian Distribution"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Team di rilascio per &ldquo;stable&rdquo;"

#~ msgid "Accountant"
#~ msgstr "Ragioniere"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Il sistema operativo universale come Desktop"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian per le organizzazioni non-profit"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Gli amministratori responsabili dei buildd per una specifica architettura "
#~ "possono essere contattati all'indirizzo <genericemail arch@buildd.debian."
#~ "org>, per esempio <genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Il nome di ogni amministratore responsabile dei buildd puÃ² essere "
#~ "trovato su <a href=\"http://www.buildd.net\">http://www.buildd.net</a>. "
#~ "Scegliere una architetura e una distribuzione per conoscere i buildd "
#~ "disponibili e i relativi amministratori."

#~ msgid "APT Team"
#~ msgstr "Team APT"

#~ msgid "Marketing Team"
#~ msgstr "Team Marketing"

#~ msgid "Auditor"
#~ msgstr "Auditor"
