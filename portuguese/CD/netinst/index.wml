#use wml::debian::cdimage title="Instala��o via rede a partir de um CD m�nimo"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="40eeef5c10b97819abf8467d7f2a8038a8729ff8" maintainer="Felipe Augusto van de Wiel (faw)"

<p>Um CD de <q>instala��o via rede</q> ou <q>netinst</q> � um �nico CD que
permite-lhe instalar todo o sistema operacional. Este �nico CD
cont�m apenas a quantidade m�nima de software para come�ar a
instala��o e obter os outros pacotes atrav�s da Internet.</p>

<p><strong>O que � melhor para mim &mdash; o CD-ROM inicializ�vel m�nimo
ou os CDs completos?</strong> Depende, mas achamos que em muitos casos
a imagem do CD m�nimo � melhor &mdash; antes de mais nada, voc� s� vai baixar
os pacotes que selecionar para a instala��o na sua m�quina, o que 
economiza tempo e banda. Por outro lado, os CDs completos s�o mais
�teis quando for instalar em mais de uma m�quina, ou m�quinas sem
acesso livre � Internet.</p>

<p><strong>Quais tipos de conex�es de rede s�o suportadas
durante a instala��o?</strong>
A instala��o via rede assume que voc� tenha acesso � Internet. V�rias formas
diferentes s�o suportadas para isso, como uma conex�o discada anal�gica PPP,
ethernet, WLAN (com algumas restri��es), mas ISDN n�o � suportada &mdash;
desculpe!</p>

<p>As seguintes imagens inicializ�veis do CD m�nimo est�o dispon�veis para
baixar:</p>

<ul>

  <li>Imagens <q>netinst</q> oficiais para a distribui��o est�vel
  (<q>stable</q>) &mdash; <a href="#netinst-stable">veja abaixo</a></li>

  <li>Imagens para a distribui��o teste (<q>testing</q>), tanto as
  constru��es di�rias como os snapshots que est�o funcionando, veja a
  <a href="$(DEVEL)/debian-installer/">p�gina do
  Debian-Installer</a>.</li>

</ul>


<h2 id="netinst-stable">Imagens netinst oficiais para a distribui��o
est�vel (<q>stable</q>)</h2>

<p>Com at� 300&nbsp;MB de tamanho, essa imagem cont�m o instalador e
um conjunto reduzido de pacotes, o que possibilita a instala��o de um sistema
(muito) b�sico.</p>

<div class="line">
<div class="item col50">
<p><strong>Imagem do CD netinst (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="item col50 lastcol">
<p><strong>Imagem do CD netinst (geralmente entre 150-300 MB, varia por arquitetura)</strong></p>
    <stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Para informa��es sobre o que s�o estes arquivos e como utiliz�-los, por favor
consulte o <a href="../faq/">FAQ</a>.</p>

<p>Uma vez que tenha baixado uma dessas imagens, n�o esque�a de dar uma olhada
nas <a href="$(HOME)/releases/stable/installmanual">informa��es detalhadas
sobre o processo de instala��o</a>.</p>
